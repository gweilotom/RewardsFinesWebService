package com.rclinz.rewardsfines.players.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model representation of the 'players' database table.
 */
@Entity
@Table(name = "players")
public class PlayersModel {

    // Id field
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    // Username field
    @Column(name = "username")
    private String username;
    // Name field
    @Column(name = "name")
    private String name;
    // Surname field
    @Column(name = "surname")
    private String surname;
    // Sex field
    @Column(name = "sex")
    private Character sex;
    // Youth field
    @Column(name = "youth")
    private Boolean youth;
    // Date of birth field
    @Column(name = "dateofbirth")
    private Date dateofbirth;
    
    /**
     * Bare constructor for model instance.
     */
    public PlayersModel() {
        this.youth = false;
    }

    /**
     * Username getter function.
     * @return
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Id getter function.
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Name getter function.
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Name setter function.
     * @param aName
     */
    public void setName(String aName) {
        this.name = aName;
    }

    /**
     * Surname getter function.
     * @return
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     * Surname setter function.
     * @param aSurname
     */
    public void setSurname(String aSurname) {
        this.surname = aSurname;
    }

    /**
     * Sex getter function.
     * @return
     */
    public Character getSex() {
        return this.sex;
    }

    /**
     * Sex setter function.
     * @param aSex
     */
    public void setSex(Character aSex) {
        this.sex = aSex;
    }

    /**
     * Youth getter function.
     * @return
     */
    public Boolean isYouth() {
        return this.youth;
    }

    /**
     * Youth setter function.
     * @param aYouth
     */
    public void setYouth(Boolean aYouth) {
        this.youth = aYouth;
    }

    /**
     * Date of birth getter function.
     * @return
     */
    public Date getDateOfBirth() {
        return this.dateofbirth;
    }

    /**
     * Date of birth setter function.
     * @param aDateOfBirth
     */
    public void setDateOfBirth(Date aDateOfBirth) {
        this.dateofbirth = aDateOfBirth;
    }

    /**
     * Creates string representation of player instance.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("username='").append(this.username).append('\'');
        sb.append(", name=").append(this.surname).append(", ").append(this.name);
        sb.append(", sex=").append(this.sex);
        sb.append(", isYouth=").append(this.youth);
        sb.append(", dateOfBirth=").append(this.dateofbirth);
        sb.append('}');
        return sb.toString();
    }
}