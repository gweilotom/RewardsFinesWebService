package com.rclinz.rewardsfines.players.services;

import java.util.List;

import com.rclinz.rewardsfines.players.interfaces.PlayersInterface;
import com.rclinz.rewardsfines.players.models.PlayersModel;
import com.rclinz.rewardsfines.players.repositories.PlayersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to controll access to 'players' table in database.
 */
@Service
public class PlayersService implements PlayersInterface {
    // Players repository for database access
    @Autowired
    private PlayersRepository playersRepo;

    /**
     * Service call to find all players stored in database.
     */
    @Override
    public List<PlayersModel> findAll() {
        return (List<PlayersModel>) playersRepo.findAll();
    }

    /**
     * Service call to find players by name.
     */
    @Override
    public List<PlayersModel> findByName(String aName) {
        return (List<PlayersModel>) playersRepo.findByName(aName);
    }

    /**
     * Service call to find players by surname.
     */
    @Override
    public List<PlayersModel> findBySurname(String aSurname) {
        return (List<PlayersModel>) playersRepo.findBySurname(aSurname);
    }

    /**
     * Service call to find a player by his/her username.
     */
    @Override
    public PlayersModel findByUsername(String aUsername) {
        return (PlayersModel) playersRepo.findByUsername(aUsername);
    }

    /**
     * Service call to find players by their full name.
     */
    @Override
    public PlayersModel findByFullName(String aName, String aSurname) {
        return (PlayersModel) playersRepo.findByNameAndSurname(aName, aSurname);
    }

    /**
     * Service call to create a new player in database.
     */
    @Override
    public PlayersModel createPlayer(PlayersModel aPlayer) {
        return (PlayersModel) playersRepo.save(aPlayer);
    }

    /**
     * Service call to update a exising player in database.
     */
    @Override
    public PlayersModel updatePlayer(PlayersModel aPlayer) {
        return (PlayersModel) playersRepo.save(aPlayer);
    }

    /**
     * Service call to delete a player by his/her name in database.
     */
    @Override
    public Long deletePlayerByFullName(String aName, String aSurname) {
        return (Long) this.playersRepo.deleteByNameAndSurname(aName, aSurname);
    }
}