package com.rclinz.rewardsfines.players.interfaces;

import java.util.List;

import com.rclinz.rewardsfines.players.models.PlayersModel;

/**
 * Interface for PlayersService.
 */
public interface PlayersInterface {

    /**
     * Interface function call to find all players in the database.
     * @return
     */
    public List<PlayersModel> findAll();

    /**
     * Interface function call to find players by name.
     * @param aName
     * @return
     */
    public List<PlayersModel> findByName(String aName);

    /**
     * Interface function call to find players by surname.
     * @param aSurname
     * @return
     */
    public List<PlayersModel> findBySurname(String aSurname);

    /**
     * Interface function call to find a player by username.
     * @param aUsername
     * @return
     */
    public PlayersModel findByUsername(String aUsername);

    /**
     * Interface function call to find players by fullname.
     * @param aName
     * @param aSurname
     * @return
     */
    public PlayersModel findByFullName(String aName, String aSurname);

    /**
     * Interface function call to create a new player in database.
     * @param aPlayer
     * @return
     */
    public PlayersModel createPlayer(PlayersModel aPlayer);

    /**
     * Interface function call to update a existing player in database.
     * @param aPlayer
     * @return
     */
    public PlayersModel updatePlayer(PlayersModel aPlayer);

    /**
     * Interface function call to delete a player by his/her fullname in database.
     * @param aName
     * @param aSurname
     * @return
     */
    public Long deletePlayerByFullName(String aName, String aSurname);
}