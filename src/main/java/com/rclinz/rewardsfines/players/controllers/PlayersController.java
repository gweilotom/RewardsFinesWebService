package com.rclinz.rewardsfines.players.controllers;

import java.util.ArrayList;
import java.util.List;

import com.rclinz.rewardsfines.players.interfaces.PlayersInterface;
import com.rclinz.rewardsfines.players.models.PlayersModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API controller for accessing and manipulating player data in the database.
 */
@RestController
public class PlayersController {

    // Interface for player access
    @Autowired
    private PlayersInterface playersInterface;

    /**
     * REST call to retrieve all players from the database.
     */
    @GetMapping(path = "/getAllPlayers", produces = "application/json")
    public ResponseEntity<List<PlayersModel>> getAllPlayers() {
        List<PlayersModel> players = playersInterface.findAll();
        if (players.size() == 0) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(players);
        }
    }

    /**
     * REST call to retrieve players by his/her name.
     * @return
     */
    @PostMapping(path = "/getPlayerByName", produces = "application/json")
    public ResponseEntity<List<PlayersModel>> getPlayerByName(@RequestBody PlayersModel aPlayer) {
        if (aPlayer.getName() == null && aPlayer.getSurname() == null) {
            return this.getAllPlayers();
        } else if (aPlayer.getName() == null && aPlayer.getSurname() != null) {
            List<PlayersModel> players = this.playersInterface.findBySurname(aPlayer.getSurname());
            if (players.size() == 0) {
                return ResponseEntity.notFound().build();
            } else {
                return ResponseEntity.ok(players);
            }
        } else if (aPlayer.getName() != null && aPlayer.getSurname() == null) {
            List<PlayersModel> players = this.playersInterface.findByName(aPlayer.getName());
            if (players.size() == 0) {
                return ResponseEntity.notFound().build();
            } else {
                return ResponseEntity.ok(players);
            }
        } else {
            PlayersModel player = this.playersInterface.findByFullName(aPlayer.getName(), aPlayer.getSurname());
            if (player == null) {
                return ResponseEntity.notFound().build();
            } else {
                List<PlayersModel> data = new ArrayList<PlayersModel>();
                data.add(player);
                return ResponseEntity.ok(data);
            }
        }
    }

    /**
     * REST call to retrieve a player by his/her username.
     * @return
     */
    @PostMapping(path = "/getPlayerByUsername", produces = "application/json")
    public ResponseEntity<PlayersModel> getPlayerByUsername(@RequestBody PlayersModel aPlayer) {
        PlayersModel player = playersInterface.findByUsername(aPlayer.getUsername());
        if (player == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(player);
        }
    }

    /**
     * REST call to create a new player, or update a existing one.
     * @param player
     * @return
     */
    @PostMapping(path = "/createUpdatePlayer", consumes = "application/json", produces = "application/json")
    public ResponseEntity<PlayersModel> createUpdatePlayer(@RequestBody PlayersModel aPlayer) {
        PlayersModel checkPlayer =
            this.playersInterface.findByFullName(aPlayer.getName(), aPlayer.getSurname());
        if (checkPlayer != null) {
            if (aPlayer.isYouth() != null) {
                checkPlayer.setYouth(aPlayer.isYouth());
            }
            if(aPlayer.getSex() != null) {
                checkPlayer.setSex(aPlayer.getSex());
            }
            PlayersModel updatedPlayer = this.playersInterface.updatePlayer(checkPlayer);
            if (updatedPlayer == null) {
                return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
            } else {
                return ResponseEntity.ok(updatedPlayer);
            }
        } else {
            PlayersModel newPlayer = this.playersInterface.createPlayer(aPlayer);
            if (newPlayer == null) {
                return ResponseEntity.status(HttpStatus.NOT_EXTENDED).build();
            } else {
                return ResponseEntity.ok(newPlayer);
            }
        }
    }

    /**
     * REST call to delete a player by their name.
     * @param aName
     * @param aSurname
     * @return
     */
    @DeleteMapping(path = "/deletePlayer", produces = "application/json")
    public ResponseEntity<Long> deletePlayer(@RequestParam(value = "name", required = true) String aName, @RequestParam(value = "surname", required = true) String aSurname) {
        PlayersModel checkPlayer = this.playersInterface.findByFullName(aName, aSurname);
        if (checkPlayer != null) {
            Long state = this.playersInterface.deletePlayerByFullName(aName, aSurname);
            if (state == 1) {
                return ResponseEntity.ok(state);
            } else if (state == 0) {
                return ResponseEntity.notFound().build();
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}