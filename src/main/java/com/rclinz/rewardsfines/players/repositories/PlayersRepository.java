package com.rclinz.rewardsfines.players.repositories;

import java.util.List;

import com.rclinz.rewardsfines.players.models.PlayersModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository to access 'players' table in database.
 */
@Repository
public interface PlayersRepository extends CrudRepository<PlayersModel, Long> {
    
    /**
     * Repository function call to find players by their name.
     * @param aName
     * @return
     */
    public List<PlayersModel> findByName(String name);

    /**
     * Repository function call to find players by their surname.
     * @param surname
     * @return
     */
    public List<PlayersModel> findBySurname(String surname);

    /**
     * Repository function call to find a player by his/her username.
     * @param username
     * @return
     */
    public PlayersModel findByUsername(String username);

    /**
     * Repository function call to find players by their full name.
     * @param name
     * @param surname
     * @return
     */
    public PlayersModel findByNameAndSurname(String name, String surname);

    /**
     * Repository function call to delete a player by their fullname.
     * @param name
     * @param surname
     * @return
     */
    @Transactional
    public Long deleteByNameAndSurname(String name, String surname);
}