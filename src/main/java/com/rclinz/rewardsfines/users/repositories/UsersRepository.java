package com.rclinz.rewardsfines.users.repositories;

import com.rclinz.rewardsfines.users.models.UsersModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository to access 'users' table in database.
 */
@Repository
public interface UsersRepository extends CrudRepository<UsersModel, Long> {
    
    /**
     * Repository function call to find a user by his/her username.
     * @param username Username of the user to be found in the database
     * @return UsersModel instance containing the users data
     */
    public UsersModel findByUsername(String username);

    /**
     * Repository function call to login with a specific username and password combination.
     * @param username
     * @param password
     * @return
     */
    public UsersModel findByUsernameAndPassword(String username, String password);

    /**
     * Repository function call to find a user by his/her username.
     * @param email Email address of the user to be found in the database
     * @return UsersModel instance containing the users data
     */
    public UsersModel findByEmail(String email);

    /**
     * Repository function call to login with a specific email and password combination.
     * @param username
     * @param password
     * @return
     */
    public UsersModel findByEmailAndPassword(String username, String password);

    /**
     * Repository function call to delete a user by his/her username.
     * @param username Username of the user to be deleted
     * @return Long value determining the success of the operation
     */
    @Transactional
    public Long deleteByUsername(String username);
}