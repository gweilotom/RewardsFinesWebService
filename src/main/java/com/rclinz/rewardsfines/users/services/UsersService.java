package com.rclinz.rewardsfines.users.services;

import com.rclinz.rewardsfines.users.models.UsersModel;
import com.rclinz.rewardsfines.users.repositories.UsersRepository;
import com.rclinz.rewardsfines.users.interfaces.UsersInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to controll access to 'users' table in database.
 */
@Service
public class UsersService implements UsersInterface {
    
    // Users repository for database access
    @Autowired
    private UsersRepository usersRepo;

    /**
     * Service call to find a specific user by his/her username.
     */
    @Override
    public UsersModel findByUsername(String aUsername) {
        return (UsersModel) usersRepo.findByUsername(aUsername);
    }

    /**
     * Service call to login with a username and password combination.
     */
    @Override
    public UsersModel loginWithUsername(String aUsername, String aPassword) {
        return (UsersModel) usersRepo.findByUsernameAndPassword(aUsername, aPassword);
    }

    /**
     * Service call to find a specific user by his/her email address.
     */
    @Override
    public UsersModel findByEmail(String email) {
        return (UsersModel) usersRepo.findByEmail(email);
    }

    /**
     * Service call to login with a email and password combination.
     */
    @Override
    public UsersModel loginWithEmail(String aEmail, String aPassword) {
        return (UsersModel) usersRepo.findByEmailAndPassword(aEmail, aPassword);
    }

    /**
     * Service call to delete a user by his/her username.
     */
    @Override
    public Long deleteUserByUsername(String username) {
        return usersRepo.deleteByUsername(username);
    }

    /**
     * Service call to create a new user in database.
     */
    @Override
    public UsersModel createUser(UsersModel user) {
        UsersModel checkUser = findByUsername(user.getUsername());
        if (checkUser == null) {
            checkUser = findByEmail(user.getEmail());
            if (checkUser == null) {
                return (UsersModel) usersRepo.save(user);
            }
        }
        return null;
    }

    /**
     * Service call to update a existing user in database.
     */
    @Override
    public UsersModel updateUser(UsersModel user) {
        UsersModel checkUser = findByUsername(user.getUsername());
        if (checkUser == null) {
            return null;
        }
        return (UsersModel) usersRepo.save(user);
    }
}