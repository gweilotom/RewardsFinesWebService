package com.rclinz.rewardsfines.users.interfaces;

import com.rclinz.rewardsfines.users.models.UsersModel;

/**
 * Interface for UsersService.
 */
public interface UsersInterface {
    
    /**
     * Interface function call to find all users in the database.
     * @return
     */
    // public List<UsersModel> findAll();

    /**
     * Interface function call to find a user by his/her username.
     * @param username
     * @return
     */
    public UsersModel findByUsername(String aUsername);

    /**
     * Interface function call to login using username and password.
     * @param aUsername
     * @param aPassword
     * @return
     */
    public UsersModel loginWithUsername(String aUsername, String aPassword);

    /**
     * Interface function call to find a user by his/her email address.
     * @param email
     * @return
     */
    public UsersModel findByEmail(String aEmail);

    /**
     * Interface function call to login using email and password.
     * @param aEmail
     * @param aPassword
     * @return
     */
    public UsersModel loginWithEmail(String aEmail, String aPassword);

    /**
     * Interface function call to delete a user by his/her username.
     * @param username
     * @return
     */
    public Long deleteUserByUsername(String username);

    /**
     * Interface function call to create a new user.
     * @param user
     * @return
     */
    public UsersModel createUser(UsersModel user);

    /**
     * Interface function call to update a existing user.
     * @param user
     * @return
     */
    public UsersModel updateUser(UsersModel user);
}