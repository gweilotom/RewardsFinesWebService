package com.rclinz.rewardsfines.users.controllers;

import com.rclinz.rewardsfines.users.models.UsersModel;
import com.rclinz.rewardsfines.users.interfaces.UsersInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API controller for accessing and manipulating user data on the database.
 */
@RestController
public class UsersController {
    
    // Interface for user access
    @Autowired
    private UsersInterface usersInterface;

    /**
     * REST call to login using a specific username and password combination.
     * @param aUsername
     * @param aPassword
     * @return
     */
    @CrossOrigin
    @PostMapping(path = "/loginWithUsername", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UsersModel> loginWithUsername(@RequestBody UsersModel aUser) {
        UsersModel user = usersInterface.loginWithUsername(aUser.getUsername(), aUser.getPassword());
        if (user != null) {
            return ResponseEntity.ok(user);
        }
        return ResponseEntity.notFound().build();
    }

    /**
     * REST call to login using a specific email and password combination.
     * @param aEmail
     * @param aPassword
     * @return
     */
    @CrossOrigin
    @PostMapping(path = "/loginWithEmail", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UsersModel> loginWithEmail(@RequestBody UsersModel aUser) {
        UsersModel user = usersInterface.loginWithEmail(aUser.getEmail(), aUser.getPassword());
        if (user != null) {
            return ResponseEntity.ok(user);
        }
        return ResponseEntity.notFound().build();
    }

    /**
     * REST call to create a new user.
     * @param user
     * @return
     */
    @CrossOrigin
    @PostMapping(path = "/createUser", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UsersModel> createUser(@RequestBody UsersModel user) {
        UsersModel createdUser = usersInterface.createUser(user);
        if (createdUser != null) {
            return ResponseEntity.ok(createdUser);
        }
        return ResponseEntity.status(HttpStatus.NOT_EXTENDED).build();
    }

    /**
     * REST call to update an existing user.
     * @param user
     * @return
     */
    @CrossOrigin
    @PutMapping(path = "/updateUser", produces = "application/json")
    public ResponseEntity<UsersModel> updateUser(@RequestBody UsersModel user) {
        UsersModel updatedUser = usersInterface.updateUser(user);
        if (updatedUser == null) {
            return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
        }
        return ResponseEntity.ok(updatedUser);
    }

    /**
     * REST call to delete a user by his/her username.
     */
    @CrossOrigin
    @DeleteMapping(path = "/deleteUser", produces = "application/json")
    public ResponseEntity<Long> deleteUser(@RequestParam(value = "username", required = true) String username) {
        Long state = usersInterface.deleteUserByUsername(username);
        if (state == 1) {
            return ResponseEntity.ok(state);
        } else if (state == 0) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}