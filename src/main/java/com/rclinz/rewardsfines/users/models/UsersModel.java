package com.rclinz.rewardsfines.users.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model representation of the 'users' database table.
 */
@Entity
@Table(name = "users")
public class UsersModel {

    // Username field
    @Id
    @Column(name = "username")
    private String username;

    // Name field
    @Column(name = "name")
    private String name;
    // Surname field
    @Column(name = "surname")
    private String surname;
    // Password field
    @Column(name = "password")
    private String password;
    // Executive field
    @Column(name = "executive")
    private Boolean executive;
    // Email field
    @Column(name = "email")
    private String email;
    // Language field
    @Column(name = "language")
    private String language;

    /**
     * Bare constructor for model instance.
     */
    public UsersModel() {
    }

    /**
     * Username getter function.
     * @return
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Name getter function.
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Name setter function.
     * @param aName
     */
    public void setName(String aName) {
        this.name = aName;
    }

    /**
     * Surname getter function.
     * @return
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     * Surname setter function.
     * @param aSurname
     */
    public void setSurname(String aSurname) {
        this.surname = aSurname;
    }

    /**
     * Password getter function.
     * @return
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Password setter function.
     * @param aPassword
     */
    public void setPassword(String aPassword) {
        this.password = aPassword;
    }

    /**
     * Executive getter function.
     * @return
     */
    public Boolean isExecutive() {
        return this.executive;
    }

    /**
     * Executive setter function.
     * @param aExecutive
     */
    public void setExecutive(Boolean aExecutive) {
        this.executive = aExecutive;
    }

    /**
     * Email getter function.
     * @return
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Email setter function.
     */
    public void setEmail(String aEmail) {
        this.email = aEmail;
    }

    /**
     * Language getter function.
     * @return
     */
    public String getLanguage() {
        return this.language;
    }

    /**
     * Language setter function.
     * @param aLanguage
     */
    public void setLanguage(String aLanguage) {
        this.language = aLanguage;
    }

    /**
     * Creates hash code of the user.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.username);
        return hash;
    }

    /**
     * Compares user instance to another object.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UsersModel other = (UsersModel) obj;
        if (this.username != other.username) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        return Objects.equals(this.username, other.username);
    }

    /**
     * Creates a string representation of the user instance.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("username='").append(this.username).append('\'');
        sb.append(", name=").append(this.surname).append(", ").append(this.name);
        sb.append(", email=").append(this.email);
        sb.append(", isExecutive=").append(this.executive);
        sb.append(", language=").append(this.language);
        sb.append('}');
        return sb.toString();
    }
}