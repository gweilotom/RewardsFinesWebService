package com.rclinz.rewardsfines.fines.interfaces;

import java.util.List;

import com.rclinz.rewardsfines.fines.models.FinesOverviewModel;


/**
 * Interface for FinesOverviewService.
 */
public interface FinesOverviewInterface {
    
    /**
     * Interface function call to find all fine overview data.
     * @return
     */
    public List<FinesOverviewModel> findAll();
}