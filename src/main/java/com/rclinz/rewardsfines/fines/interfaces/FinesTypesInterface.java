package com.rclinz.rewardsfines.fines.interfaces;

import java.util.List;

import com.rclinz.rewardsfines.fines.models.FinesTypesModel;

/**
 * Interface for fine type service.
 */
public interface FinesTypesInterface {

    /**
     * Interface function call to find all fine types in the database.
     * @return
     */
    public List<FinesTypesModel> findAll();

    /**
     * Interface function call to find a fine type by its id in the database.
     * @param aId
     * @return
     */
    public FinesTypesModel findById(Long aId);

    /**
     * Interface function call to create a new fine type in the database.
     * @param aFineType
     * @return
     */
    public FinesTypesModel createFineType(FinesTypesModel aFineType);

    /**
     * Interface function call to update an existing fine type in the database.
     * @param aFineType
     * @return
     */
    public FinesTypesModel updateFineType(FinesTypesModel aFineType);
    
    /**
     * Interface function call to delete a fine type from the database.
     * @param aId
     * @return
     */
    public Long deleteFineTypeById(Long aId);
}