package com.rclinz.rewardsfines.fines.interfaces;

import java.util.List;

import com.rclinz.rewardsfines.fines.models.FinesListModel;

public interface FinesListInterface {
    /**
     * Interface function call to find a fine by its id.
     * @param aId
     * @return
     */
    public FinesListModel findById(Long aId);

    /**
     * Interface function call to find all fines attached to a player.
     * @param aPlayerId
     * @return
     */
    public List<FinesListModel> findByPlayerId(Long aPlayerId);

    /**
     * Interface function call to find fines of a certain type attached to a player.
     * @param aPlayerId
     * @param aFineTypeId
     * @return
     */
    public List<FinesListModel> findByPlayerIdAndFineTypeId(Long aPlayerId, Long aFineTypeId);

    /**
     * Interface function call to attach a new fine to a player.
     * @param aPlayerId
     * @param aFineTypeId
     * @return
     */
    public FinesListModel createFineForPlayer(FinesListModel aFine);

    /**
     * Interface function call to mark a specific fine as paid.
     * @param aFineId
     * @return
     */
    public FinesListModel finePaid(FinesListModel aFine);

    /**
     * Interface function call to delete a specific fine.
     * @param aFineId
     * @return
     */
    public Long deleteFine(Long aFineId);    
}