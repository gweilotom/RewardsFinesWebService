package com.rclinz.rewardsfines.fines.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model representation of the 'fines_list' database table.
 */
@Entity
@Table(name = "fines_list")
public class FinesListModel {

    // Id field
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Fine id field
    @Column(name = "fineid")
    private Long fineid;
    // Player id field
    @Column(name = "playerid")
    private Long playerid;
    // Fine date field
    @Column(name = "finedate")
    private Date finedate;
    // Fine paid field
    @Column(name = "finepaid")
    private Boolean finepaid;

    /**
     * Bare constructor for model instance.
     */
    public FinesListModel() {
        this.finepaid = false;
    }

    /**
     * Id getter function.
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Fine id getter function.
     * @return
     */
    public Long getFineId() {
        return this.fineid;
    }

    /**
     * Fine id setter function.
     * @param aFineId
     */
    public void setFineId(Long aFineId) {
        this.fineid = aFineId;
    }

    /**
     * Player id getter function.
     * @return
     */
    public Long getPlayerId() {
        return this.playerid;
    }

    /**
     * Player id setter function.
     * @param aPlayerId
     */
    public void setPlayerId(Long aPlayerId) {
        this.playerid = aPlayerId;
    }

    /**
     * Fine date getter function.
     * @return
     */
    public Date getFineDate() {
        return this.finedate;
    }

    /**
     * Fine date setter function.
     * @param aFineDate
     */
    public void setFineDate(Date aFineDate) {
        this.finedate = aFineDate;
    }

    /**
     * Fine paid getter function.
     * @return
     */
    public Boolean isFinePaid() {
        return this.finepaid;
    }

    /**
     * Fine paid setter function.
     * @param aFinePaid
     */
    public void setFinePaid(Boolean aFinePaid) {
        this.finepaid = aFinePaid;
    }

    /**
     * Returns fine list instance as a String.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("id='").append(this.id).append('\'');
        sb.append(", fineid=").append(this.fineid);
        sb.append(", playerid=").append(this.playerid);
        sb.append(", finedate=").append(this.finedate);
        sb.append(", finepaid=").append(this.finepaid);
        sb.append("}");
        return sb.toString();
    }
}