package com.rclinz.rewardsfines.fines.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model representation of the 'fine_types' database table.
 */
@Entity
@Table(name = "fines_types")
public class FinesTypesModel {
    
    // Id field
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Description field
    @Column(name = "description")
    private String description;
    // Monetary_1st field
    @Column(name = "monetary_1st")
    private Long monetary_1st;
    // Points_1st field
    @Column(name = "points_1st")
    private Long points_1st;
    // Monetary_2nd field
    @Column(name = "monetary_2nd")
    private Long monetary_2nd;
    // Points_2nd field
    @Column(name = "points_2nd")
    private Long points_2nd;
    // Monetary_3rd field
    @Column(name = "monetary_3rd")
    private Long monetary_3rd;
    // Points_3rd field
    @Column(name = "points_3rd")
    private Long points_3rd;
    // Rising field
    @Column(name = "rising")
    private Boolean rising;

    /**
     * Bare constructor for model instance.
     */
    public FinesTypesModel() {
        this.rising = false;
    }

    /**
     * Id getter function.
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Description getter function.
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Description setter function.
     * @param aDescription
     */
    public void setDescription(String aDescription) {
        this.description = aDescription;
    }

    /**
     * Monetary 1st getter function.
     * @return
     */
    public Long getMonetary1st() {
        return this.monetary_1st;
    }

    /**
     * Monetary 1st setter function.
     * @param aMonetary1st
     */
    public void setMonetary1st(Long aMonetary1st) {
        this.monetary_1st = aMonetary1st;
    }

    /**
     * Points 1st getter function.
     * @return
     */
    public Long getPoints1st() {
        return this.points_1st;
    }

    /**
     * Points 1st setter function.
     * @param aPoints1st
     */
    public void setPoints1st(Long aPoints1st) {
        this.points_1st = aPoints1st;
    }

    /**
     * Monetary 2nd getter function.
     * @return
     */
    public Long getMonetary2nd() {
        return this.monetary_2nd;
    }

    /**
     * Monetary 2nd setter function.
     * @param aMonetary2nd
     */
    public void setMonetary2nd(Long aMonetary2nd) {
        this.monetary_2nd = aMonetary2nd;
    }

    /**
     * Points 2nd getter function.
     * @return
     */
    public Long getPoints2nd() {
        return this.points_2nd;
    }

    /**
     * Points 2nd setter function.
     * @param aPoints2nd
     */
    public void setPoints2nd(Long aPoints2nd) {
        this.points_2nd = aPoints2nd;
    }

    /**
     * Monetary 3rd getter function.
     * @return
     */
    public Long getMonetary3rd() {
        return this.monetary_3rd;
    }

    /**
     * Monetary 3rd setter function.
     * @param aMonetary1st
     */
    public void setMonetary3rd(Long aMonetary3rd) {
        this.monetary_3rd = aMonetary3rd;
    }

    /**
     * Points 3rd getter function.
     * @return
     */
    public Long getPoints3rd() {
        return this.points_3rd;
    }

    /**
     * Points 3rd setter function.
     * @param aPoints3rd
     */
    public void setPoints3rd(Long aPoints3rd) {
        this.points_1st = aPoints3rd;
    }

    /**
     * Rising getter function.
     * @return
     */
    public Boolean getRising() {
        return this.rising;
    }

    /**
     * Rising setter function.
     * @param aRising
     */
    public void setRising(Boolean aRising) {
        this.rising = aRising;
    }

    /**
     * Returns fine type instance as a String.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("id='").append(this.id).append('\'');
        sb.append(", description=").append(this.description);
        sb.append(", monetary1st=").append(this.monetary_1st);
        sb.append(", points1st=").append(this.points_1st);
        sb.append(", monetary2nd=").append(this.monetary_2nd);
        sb.append(", points2nd=").append(this.points_2nd);
        sb.append(", monetary3rd=").append(this.monetary_3rd);
        sb.append(", points3rd=").append(this.points_3rd);
        sb.append(", rising=").append(this.rising);
        sb.append('}');
        return sb.toString();
    }
}