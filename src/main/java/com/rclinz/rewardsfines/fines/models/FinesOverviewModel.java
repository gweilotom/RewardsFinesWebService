package com.rclinz.rewardsfines.fines.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model representation of the 'fines_ovv_mvd' database materialized view.
 */
@Entity
@Table(name = "fines_ovv_mvd")
public class FinesOverviewModel {
    
    // Player id field
    @Id
    @Column(name = "playerid")
    private Long playerid;
    
    // Name field
    @Column(name = "name")
    private String name;
    // Surname field
    @Column(name = "surname")
    private String surname;
    // Username field
    @Column(name = "username")
    private String username;
    // Sex field
    @Column(name = "sex")
    private Character sex;
    // Youth field
    @Column(name = "youth")
    private Boolean youth;
    // Monetary field
    @Column(name = "monetary")
    private Long monetary;
    // Points field
    @Column(name = "points")
    private Long points;

    /**
     * Bare constructor for model instance.
     */
    public FinesOverviewModel() {
        this.youth = false;
    }

    /**
     * Player id getter function.
     * @return
     */
    public Long getPlayerId() {
        return this.playerid;
    }

    /**
     * Name getter function.
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Surname getter function.
     * @return
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     * Username getter function.
     * @return
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sex getter function.
     * @return
     */
    public Character getSex() {
        return this.sex;
    }

    /**
     * Youth getter function.
     * @return
     */
    public Boolean isYouth() {
        return this.youth;
    }

    /**
     * Monetary getter function.
     * @return
     */
    public Long getMonetary() {
        return this.monetary;
    }

    /**
     * Points getter function.
     * @return
     */
    public Long getPoints() {
        return this.points;
    }

    /**
     * Returns fine overview instance as a String.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("playerid='").append(this.playerid).append('\'');
        sb.append(", name=").append(this.name);
        sb.append(", surname=").append(this.surname);
        sb.append(", username=").append(this.username);
        sb.append(", sex=").append(this.sex);
        sb.append(", youth=").append(this.youth);
        sb.append(", monetary=").append(this.monetary);
        sb.append(", points=").append(this.points);
        sb.append('}');
        return sb.toString();
    }
}