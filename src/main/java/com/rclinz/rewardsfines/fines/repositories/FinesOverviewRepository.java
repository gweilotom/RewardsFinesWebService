package com.rclinz.rewardsfines.fines.repositories;

import com.rclinz.rewardsfines.fines.models.FinesOverviewModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository to access 'fines_ovv_mvd' materialised view in database.
 */
@Repository
public interface FinesOverviewRepository extends CrudRepository<FinesOverviewModel, Long> {}