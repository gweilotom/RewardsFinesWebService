package com.rclinz.rewardsfines.fines.repositories;

import javax.transaction.Transactional;

import com.rclinz.rewardsfines.fines.models.FinesTypesModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository to access 'fines_types' table in database.
 */
@Repository
public interface FinesTypesRepository extends CrudRepository<FinesTypesModel, Long> {

    /**
     * Repository function call to find a fine type by its id.
     * @param id
     * @return
     */
    public FinesTypesModel findFineTypeById(Long id);

    /**
     * Repository function call to delete a fine type by its id.
     * @param id
     * @return
     */
    @Transactional
    public Long deleteFineTypeById(Long id);
}