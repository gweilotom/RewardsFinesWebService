package com.rclinz.rewardsfines.fines.repositories;

import java.util.List;

import com.rclinz.rewardsfines.fines.models.FinesListModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository to access 'fines_list' table in database.
 */
@Repository
public interface FinesListRepository extends CrudRepository<FinesListModel, Long> {
    
    /**
     * Repository function call to find a fine by its id.
     * @param aId
     * @return
     */
    public FinesListModel findFineById(Long aId);

    /**
     * Repository function call to find fines by a players id.
     * @param playerid
     * @return
     */
    public List<FinesListModel> findByPlayerid(Long playerid);

    /**
     * Repository function call to find fines by a players id and the fine type id.
     * @param playerid
     * @param id
     * @return
     */
    public List<FinesListModel> findByPlayeridAndFineid(Long playerid, Long id);

    /**
     * Repository function call to delete a fine by its id.
     * @param id
     * @return
     */
    @Transactional
    public Long deleteFineById(Long id);
}