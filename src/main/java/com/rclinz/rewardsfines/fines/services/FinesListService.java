package com.rclinz.rewardsfines.fines.services;

import java.util.List;

import com.rclinz.rewardsfines.fines.interfaces.FinesListInterface;
import com.rclinz.rewardsfines.fines.models.FinesListModel;
import com.rclinz.rewardsfines.fines.repositories.FinesListRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to control access to 'fines_list' table in the database.
 */
@Service
public class FinesListService implements FinesListInterface {

    // Fines list repository for database access
    @Autowired
    private FinesListRepository finesListRepo;

    /**
     * Service function call to find a fine by its id.
     */
    @Override
    public FinesListModel findById(Long aId) {
        return (FinesListModel) finesListRepo.findFineById(aId);
    }

    /**
     * Service function call to find all fines attached to a player.
     */
    @Override
    public List<FinesListModel> findByPlayerId(Long aPlayerId) {
        return (List<FinesListModel>) this.finesListRepo.findByPlayerid(aPlayerId);
    }

    /**
     * Service function call to find all fines of a specific type attached to a player.
     */
    @Override
    public List<FinesListModel> findByPlayerIdAndFineTypeId(Long aPlayerId, Long aFineTypeId) {
        return (List<FinesListModel>) this.finesListRepo.findByPlayeridAndFineid(aPlayerId, aFineTypeId);
    }

    /**
     * Service function call to attach a new fine of a specific type to a player.
     */
    @Override
    public FinesListModel createFineForPlayer(FinesListModel aFine) {
        return (FinesListModel) this.finesListRepo.save(aFine);
    }

    /**
     * Service function call to mark a specific fine as paid.
     */
    @Override
    public FinesListModel finePaid(FinesListModel aFine) {
        return (FinesListModel) this.finesListRepo.save(aFine);
    }

    /**
     * Service function call to delete a specific fine.
     */
    @Override
    public Long deleteFine(Long aFineId) {
        return (Long) this.finesListRepo.deleteFineById(aFineId);
    }
}