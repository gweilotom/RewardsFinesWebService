package com.rclinz.rewardsfines.fines.services;

import java.util.List;

import com.rclinz.rewardsfines.fines.interfaces.FinesTypesInterface;
import com.rclinz.rewardsfines.fines.models.FinesTypesModel;
import com.rclinz.rewardsfines.fines.repositories.FinesTypesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to control access to 'fines_types' table in the database.
 */
@Service
public class FinesTypesService implements FinesTypesInterface {

    // Fines type repository for database access
    @Autowired
    private FinesTypesRepository finesTypesRepo;

    /**
     * Service call to find all fine types in the database.
     */
    @Override
    public List<FinesTypesModel> findAll() {
        return (List<FinesTypesModel>) this.finesTypesRepo.findAll();
    }

    /**
     * Service call to find a fine type by its id in the database.
     */
    @Override
    public FinesTypesModel findById(Long aId) {
        return (FinesTypesModel) this.finesTypesRepo.findFineTypeById(aId);
    }

    /**
     * Service call to create a new fine type in the database.
     */
    @Override
    public FinesTypesModel createFineType(FinesTypesModel aFineType) {
        return (FinesTypesModel) this.finesTypesRepo.save(aFineType);
    }

    /**
     * Service call to update an existing fine type in the database.
     */
    @Override
    public FinesTypesModel updateFineType(FinesTypesModel aFineType) {
        return (FinesTypesModel) this.finesTypesRepo.save(aFineType);
    }

    /**
     * Service call to delete a fine type from the database.
     */
    @Override
    public Long deleteFineTypeById(Long aId) {
        return (Long) this.finesTypesRepo.deleteFineTypeById(aId);
    }   
}