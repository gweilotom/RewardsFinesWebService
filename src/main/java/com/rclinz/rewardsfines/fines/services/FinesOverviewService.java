package com.rclinz.rewardsfines.fines.services;

import java.util.List;

import com.rclinz.rewardsfines.fines.interfaces.FinesOverviewInterface;
import com.rclinz.rewardsfines.fines.models.FinesOverviewModel;
import com.rclinz.rewardsfines.fines.repositories.FinesOverviewRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to control access to 'fines_ovv_mvd' materialised view in the database.
 */
@Service
public class FinesOverviewService implements FinesOverviewInterface {

    // Fines overview repository for database access
    @Autowired
    private FinesOverviewRepository finesOverviewRepo;

    /**
     * Service function call to find all fines overview data in the database.
     */
    @Override
    public List<FinesOverviewModel> findAll() {
        return (List<FinesOverviewModel>) finesOverviewRepo.findAll();
    }
}