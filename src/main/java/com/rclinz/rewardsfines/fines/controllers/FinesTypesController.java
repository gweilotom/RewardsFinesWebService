package com.rclinz.rewardsfines.fines.controllers;

import java.util.List;

import com.rclinz.rewardsfines.fines.interfaces.FinesTypesInterface;
import com.rclinz.rewardsfines.fines.models.FinesTypesModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * REST API controller for accessing and manipulating fine type data in the database.
 */
@RestController
public class FinesTypesController {
    
    // Interface for fine types access
    @Autowired
    private FinesTypesInterface finesTypesInterface;

    /**
     * REST call to retrieve all fine types from the database.
     * @return
     */
    @GetMapping(path = "/getAllFineTypes", produces = "application/json")
    public ResponseEntity<List<FinesTypesModel>> getAllFineTypes() {
        List<FinesTypesModel> fineTypes = finesTypesInterface.findAll();
        if (fineTypes.size() == 0) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(fineTypes);
        }
    }

    /**
     * REST call to create a new, or update an existing fine type in the database.
     * @param aFineType
     * @return
     */
    @PostMapping(path = "/createUpdateFineType", consumes = "application/json", produces = "application/json")
    public ResponseEntity<FinesTypesModel> createUpdateFineType(@RequestBody FinesTypesModel aFineType) {
        FinesTypesModel checkFineType = this.finesTypesInterface.findById(aFineType.getId());
        if (checkFineType != null) {
            FinesTypesModel updatedFineType = this.finesTypesInterface.updateFineType(aFineType);
            if (updatedFineType != null) {
                return ResponseEntity.ok(updatedFineType);
            } else {
                return ResponseEntity.notFound().build();
            }
        } else {
            FinesTypesModel newFineType = this.finesTypesInterface.createFineType(aFineType);
            if (newFineType != null) {
                return ResponseEntity.ok(newFineType);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_EXTENDED).build();
            }
        }
    }
    
    /**
     * REST call to delete a fine type in the database.
     * @param aId
     * @return
     */
    @DeleteMapping(path = "/deleteFineType", produces = "application/json")
    public ResponseEntity<Long> deleteFineType(@RequestParam(value = "id", required = true) Long aId) {
        FinesTypesModel checkFineType = this.finesTypesInterface.findById(aId);
        if (checkFineType != null) {
            Long state = this.finesTypesInterface.deleteFineTypeById(aId);
            if (state == 1) {
                return ResponseEntity.ok(state);
            } else if (state == 0) {
                return ResponseEntity.notFound().build();
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}