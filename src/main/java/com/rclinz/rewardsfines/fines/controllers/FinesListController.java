package com.rclinz.rewardsfines.fines.controllers;

import java.util.List;

import com.rclinz.rewardsfines.fines.interfaces.FinesListInterface;
import com.rclinz.rewardsfines.fines.models.FinesListModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API controller for accessing and manipulating fine list data in the database.
 */
@RestController
public class FinesListController {

    // Interface for fines list access
    @Autowired
    private FinesListInterface finesListInterface;

    /**
     * REST call to retrieve fines for a player from the database.
     * @param aId
     * @return
     */
    @GetMapping(path = "/getFinesForPlayer", produces = "application/json")
    public ResponseEntity<List<FinesListModel>> getFinesForPlayer(@RequestParam(value = "playerid", required = true) Long aPlayerId,
        @RequestParam(value = "finetypeid", required = false) Long aFineTypeId) {
        if (aFineTypeId != null) {
            List<FinesListModel> finesList = this.finesListInterface.findByPlayerIdAndFineTypeId(aPlayerId, aFineTypeId);
            if (finesList.size() != 0) {
                return ResponseEntity.ok(finesList);
            } else {
                return ResponseEntity.notFound().build();
            }
        } else {
            List<FinesListModel> finesList = this.finesListInterface.findByPlayerId(aPlayerId);
            if (finesList.size() != 0) {
                return ResponseEntity.ok(finesList);
            } else {
                return ResponseEntity.notFound().build();
            }
        }
    }

    /**
     * REST call to attach a new fine to a player in the database.
     * @param aPlaterId
     * @param aFineId
     * @return
     */
    @PostMapping(path = "/finePlayer", consumes = "application/json", produces = "application/json")
    public ResponseEntity<FinesListModel> finePlayer(@RequestBody FinesListModel aNewFine) {
        FinesListModel newFine = this.finesListInterface.createFineForPlayer(aNewFine);
        if (newFine != null) {
            return ResponseEntity.ok(newFine);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * REST call to mark a fine as paid in the database.
     * @param aFineId
     * @return
     */
    @PostMapping(path = "/finePaid", consumes = "application/json", produces = "application/json")
    public ResponseEntity<FinesListModel> finePaid(@RequestBody FinesListModel aFinePaid) {
        FinesListModel checkFinePaid = this.finesListInterface.findById(aFinePaid.getFineId());
        if (checkFinePaid != null) {
            checkFinePaid.setFinePaid(!checkFinePaid.isFinePaid());
            FinesListModel finePaid = this.finesListInterface.finePaid(checkFinePaid);
            if (finePaid != null) {
                return ResponseEntity.ok(finePaid);
            } else {
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * REST call to delete a fine from the database.
     * @param aFineId
     * @return
     */
    @DeleteMapping(path = "/deleteFine", produces = "application/json")
    public ResponseEntity<Long> deleteFine(@RequestParam(value = "fineid", required = true) Long aFineId) {
        Long state = this.finesListInterface.deleteFine(aFineId);
        if (state == 1) {
            return ResponseEntity.ok(state);
        } else if (state == 0) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}