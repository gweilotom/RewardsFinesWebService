package com.rclinz.rewardsfines.fines.controllers;

import java.util.List;

import com.rclinz.rewardsfines.fines.interfaces.FinesOverviewInterface;
import com.rclinz.rewardsfines.fines.models.FinesOverviewModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API controller for accessing the fines overview data in the database.
 */
@RestController
public class FinesOverviewController {
    
    // Interface for fine overview access
    @Autowired
    private FinesOverviewInterface finesOverviewInterface;

    /**
     * REST call to retrieve all fine overview data.
     * @return
     */
    @GetMapping(path = "/getFinesOverview", produces = "application/json")
    public ResponseEntity<List<FinesOverviewModel>> getFinesOverview() {
        List<FinesOverviewModel> finesOverview = finesOverviewInterface.findAll();
        if (finesOverview.size() > 0) {
            return ResponseEntity.ok(finesOverview);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}