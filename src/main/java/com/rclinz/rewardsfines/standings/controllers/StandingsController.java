package com.rclinz.rewardsfines.standings.controllers;

import java.util.List;

import com.rclinz.rewardsfines.standings.interfaces.StandingsInterface;
import com.rclinz.rewardsfines.standings.models.StandingsModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API controller for accessing the standings overview data in the database.
 */
@RestController
public class StandingsController {

    // Interface for standings access
    @Autowired
    private StandingsInterface standingsInterface;

    /**
     * REST call to find all standings data in the database.
     * @return
     */
    @GetMapping(path = "/getStandings", produces = "aaplication/json")
    public ResponseEntity<List<StandingsModel>> getStandings() {
        List<StandingsModel> standings = this.standingsInterface.findAll();
        if (standings.size() > 0) {
            return ResponseEntity.ok(standings);
        }
        return ResponseEntity.noContent().build();
    }
}