package com.rclinz.rewardsfines.standings.interfaces;

import java.util.List;

import com.rclinz.rewardsfines.standings.models.StandingsModel;

/**
 * Interface for StandingsOverview.
 */
public interface StandingsInterface {
    
    /**
     * Interface function call to find all standings data.
     * @return
     */
    public List<StandingsModel> findAll();
}