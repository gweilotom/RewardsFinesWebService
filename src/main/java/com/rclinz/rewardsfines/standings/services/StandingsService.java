package com.rclinz.rewardsfines.standings.services;

import java.util.List;

import com.rclinz.rewardsfines.standings.interfaces.StandingsInterface;
import com.rclinz.rewardsfines.standings.models.StandingsModel;
import com.rclinz.rewardsfines.standings.repositories.StandingsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to control access to 'standings_ovv_mvd' materialized view in the database.
 */
@Service
public class StandingsService implements StandingsInterface {

    // Standings repository for database access
    @Autowired
    private StandingsRepository standingsRepo;
    
    /**
     * Service function call to find all standings data in the database.
     */
    @Override
    public List<StandingsModel> findAll() {
        return (List<StandingsModel>) this.standingsRepo.findAll();
    }
}