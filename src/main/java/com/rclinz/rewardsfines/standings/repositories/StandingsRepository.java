package com.rclinz.rewardsfines.standings.repositories;

import com.rclinz.rewardsfines.standings.models.StandingsModel;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository to access 'standings_mvd' materialsed view in database.
 */
public interface StandingsRepository extends CrudRepository<StandingsModel, Long> {}