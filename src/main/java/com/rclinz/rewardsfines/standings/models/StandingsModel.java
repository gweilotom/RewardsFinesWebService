package com.rclinz.rewardsfines.standings.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model representation of the 'standings_ovv_mvd' database materialized view.
 */
@Entity
@Table(name = "standings_ovv_mvd")
public class StandingsModel {
    
    // Player id field
    @Id
    @Column(name = "playerid")
    private Long playerid;

    // Name field
    @Column(name = "name")
    private String name;
    // Surname field
    @Column(name = "surname")
    private String surname;
    // Username field
    @Column(name = "username")
    private String username;
    // Sex field
    @Column(name = "sex")
    private Character sex;
    // Youth field
    @Column(name = "youth")
    private Boolean youth;
    // Fines open field
    @Column(name = "finesopen")
    private Long finesopen;
    // Fine points field
    @Column(name = "finepoints")
    private Long finepoints;
    // Reward points field
    @Column(name = "rewardpoints")
    private Long rewardpoints;
    // Fines total field
    @Column(name = "finestotal")
    private Long finestotal;
    // Points total field
    @Column(name = "pointstotal")
    private Long pointstotal;

    /**
     * Bare constructor for model instance.
     */
    public StandingsModel() {}

    /**
     * Player id getter function.
     * @return
     */
    public Long getPlayerId() {
        return this.playerid;
    }

    /**
     * Name getter function.
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Surname getter function.
     * @return
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     * Username getter function.
     * @return
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sex getter function.
     * @return
     */
    public Character getSex() {
        return this.sex;
    }

    /**
     * Youth getter function.
     * @return
     */
    public Boolean isYouth() {
        return this.youth;
    }

    /**
     * Fines open getter function.
     * @return
     */
    public Long getFinesOpen() {
        return this.finesopen;
    }

    /**
     * Fine points getter function.
     * @return
     */
    public Long getFinePoints() {
        return this.finepoints;
    }

    /**
     * Rewards getter function.
     * @return
     */
    public Long getRewardPoints() {
        return this.rewardpoints;
    }

    /**
     * Fines total getter function.
     * @return
     */
    public Long getFinesTotal() {
        return this.finestotal;
    }

    /**
     * Total points getter function.
     * @return
     */
    public Long getTotalPoints() {
        return this.pointstotal;
    }

    /**
     * Returns standings instance as a string.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("playerid='").append(this.playerid).append('\'');
        sb.append(", name=").append(this.name);
        sb.append(", surname=").append(this.surname);
        sb.append(", username=").append(this.username);
        sb.append(", sex=").append(this.sex);
        sb.append(", youth=").append(this.youth);
        sb.append(", points=").append(this.finesopen);
        sb.append(", points=").append(this.finepoints);
        sb.append(", points=").append(this.rewardpoints);
        sb.append(", points=").append(this.finestotal);
        sb.append(", points=").append(this.pointstotal);
        sb.append('}');
        return sb.toString();
    }
}