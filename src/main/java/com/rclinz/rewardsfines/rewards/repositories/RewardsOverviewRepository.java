package com.rclinz.rewardsfines.rewards.repositories;

import com.rclinz.rewardsfines.rewards.models.RewardsOverviewModel;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository to access 'rewards_ovv_mvd' materialsed view in database.
 */
public interface RewardsOverviewRepository extends CrudRepository<RewardsOverviewModel, Long> {}