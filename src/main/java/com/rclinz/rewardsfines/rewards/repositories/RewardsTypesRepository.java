package com.rclinz.rewardsfines.rewards.repositories;

import com.rclinz.rewardsfines.rewards.models.RewardsTypesModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository to access 'rewards_types' table in the database.
 */
@Repository
public interface RewardsTypesRepository extends CrudRepository<RewardsTypesModel, Long> {
    
    /**
     * Repository function call to find a reward type by its id.
     * @param id
     * @return
     */
    public RewardsTypesModel findRewardTypeById(Long id);

    /**
     * Repository function call to delete a reward type by its id.
     * @param id
     * @return
     */
    @Transactional
    public Long deleteRewardTypeById(Long id);
}