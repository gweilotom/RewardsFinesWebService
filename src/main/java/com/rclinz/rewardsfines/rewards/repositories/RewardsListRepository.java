package com.rclinz.rewardsfines.rewards.repositories;

import java.util.List;

import com.rclinz.rewardsfines.rewards.models.RewardsListModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository to access 'rewards_list' table in database.
 */
@Repository
public interface RewardsListRepository extends CrudRepository<RewardsListModel, Long> {
    
    /**
     * Repository function call to find all rewards attached to a player.
     * @param playerid
     * @return
     */
    public List<RewardsListModel> findByPlayerid(Long playerid);

    /**
     * Repository function call to find all rewards of a specific type attached to a player.
     * @param playerid
     * @param rewardid
     * @return
     */
    public List<RewardsListModel> findByPlayeridAndRewardid(Long playerid, Long rewardid);

    /**
     * Repository function call to delete a specific reward.
     * @param playerid
     * @return
     */
    @Transactional
    public Long deleteRewardById(Long playerid);
}