package com.rclinz.rewardsfines.rewards.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rewards_types")
public class RewardsTypesModel {
    
    // Id field
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Description field
    @Column(name = "description")
    private String description;
    // Points field
    @Column(name = "points")
    private Long points;

    /**
     * Bare constructor for model instance.
     */
    public RewardsTypesModel() {}

    /**
     * Id getter function.
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Description getter function.
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Description setter function.
     * @param aDescription
     */
    public void getDescription(String aDescription) {
        this.description = aDescription;
    }

    /**
     * Points getter function.
     * @return
     */
    public Long getPoints() {
        return this.points;
    }

    /**
     * Points setter function.
     * @param aPoints
     */
    public void setPoints(Long aPoints) {
        this.points = aPoints;
    }

    /**
     * Returns a string representation of the reward type instance.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("id='").append(this.id).append('\'');
        sb.append(", description=").append(this.description);
        sb.append(", points=").append(this.points);
        return sb.toString();
    }
}