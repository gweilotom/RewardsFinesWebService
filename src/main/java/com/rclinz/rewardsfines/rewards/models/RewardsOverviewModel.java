package com.rclinz.rewardsfines.rewards.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model representation of the 'rewards_ovv_mvd' database materialized view.
 */
@Entity
@Table(name = "rewards_ovv_mvd")
public class RewardsOverviewModel {
    
    // Player id field
    @Id
    @Column(name = "playerid")
    private Long playerid;

    // Name field
    @Column(name = "name")
    private String name;
    // Surname field
    @Column(name = "surname")
    private String surname;
    // Username field
    @Column(name = "username")
    private String username;
    // Sex field
    @Column(name = "sex")
    private Character sex;
    // Youth field
    @Column(name = "youth")
    private Boolean youth;
    // Points field
    @Column(name = "points")
    private Long points;

    /**
     * Bare constructor for model instance.
     */
    public RewardsOverviewModel() {}

    /**
     * Player id getter function.
     * @return
     */
    public Long getPlayerId() {
        return this.playerid;
    }

    /**
     * Name getter function.
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Surname getter function.
     * @return
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     * Username getter function.
     * @return
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sex getter function.
     * @return
     */
    public Character getSex() {
        return this.sex;
    }

    /**
     * Youth getter function.
     * @return
     */
    public Boolean isYouth() {
        return this.youth;
    }

    /**
     * Points getter function.
     * @return
     */
    public Long getPoints() {
        return this.points;
    }

    /**
     * Returns reward overview instance as a string.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("playerid='").append(this.playerid).append('\'');
        sb.append(", name=").append(this.name);
        sb.append(", surname=").append(this.surname);
        sb.append(", username=").append(this.username);
        sb.append(", sex=").append(this.sex);
        sb.append(", youth=").append(this.youth);
        sb.append(", points=").append(this.points);
        sb.append('}');
        return sb.toString();
    }
}