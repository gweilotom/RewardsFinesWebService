package com.rclinz.rewardsfines.rewards.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model representation of the 'rewards_list' table in the database.
 */
@Entity
@Table(name = "rewards_list")
 public class RewardsListModel {
    
    // Id field
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Reward id field
    @Column(name = "rewardid")
    private Long rewardid;
    // Player id field
    @Column(name = "playerid")
    private Long playerid;
    // Reward date field
    @Column(name = "rewarddate")
    private Date rewarddate;
    // Multiplier field
    @Column(name = "multiplier")
    private Long multiplier;
    // Bonud field
    @Column(name = "bonus")
    private Long bonus;

    /**
     * Bare constructor for model instance.
     */
    public RewardsListModel() {}

    /**
     * Id getter function.
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Reward id getter function.
     * @return
     */
    public Long getRewardId() {
        return this.rewardid;
    }

    /**
     * Reward id setter function.
     * @param aRewardId
     */
    public void setRewardId(Long aRewardId) {
        this.rewardid = aRewardId;
    }

    /**
     * Player id getter function.
     * @return
     */
    public Long getPlayerId() {
        return this.playerid;
    }

    /**
     * Player id setter function.
     * @param aPlayerId
     */
    public void setPlayerId(Long aPlayerId) {
        this.playerid = aPlayerId;
    }

    /**
     * Reward date getter function.
     * @return
     */
    public Date getRewardDate() {
        return this.rewarddate;
    }

    /**
     * Reward date setter function.
     * @param aRewardDate
     */
    public void setRewardDate(Date aRewardDate) {
        this.rewarddate = aRewardDate;
    }

    /**
     * Multiplier getter function.
     * @return
     */
    public Long getMultiplier() {
        return this.multiplier;
    }

    /**
     * Multiplier setter function.
     * @param aMultiplier
     */
    public void setMultiplier(Long aMultiplier) {
        this.multiplier = aMultiplier;
    }

    /**
     * Bonus getter function.
     * @return
     */
    public Long getBonus() {
        return this.bonus;
    }

    /**
     * Bonus setter function.
     */
    public void setBonus(Long aBonus) {
        this.bonus = aBonus;
    }

    /**
     * Returns rewards list instance as a string.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("id='").append(this.id).append('\'');
        sb.append(", rewardid=").append(this.rewardid);
        sb.append(", playerid=").append(this.playerid);
        sb.append(", rewarddate=").append(this.rewarddate);
        sb.append(", multiplier=").append(this.multiplier);
        sb.append(", bonus=").append(this.bonus);
        sb.append("}");
        return sb.toString();
    }
}