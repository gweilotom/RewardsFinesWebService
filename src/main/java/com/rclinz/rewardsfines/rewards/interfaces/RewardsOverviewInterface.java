package com.rclinz.rewardsfines.rewards.interfaces;

import java.util.List;

import com.rclinz.rewardsfines.rewards.models.RewardsOverviewModel;

/**
 * Interface for RewardsOverviewService.
 */
public interface RewardsOverviewInterface {
    
    /**
     * Interface function call to find all reward overview data.
     * @return
     */
    public List<RewardsOverviewModel> findAll();
}