package com.rclinz.rewardsfines.rewards.interfaces;

import java.util.List;

import com.rclinz.rewardsfines.rewards.models.RewardsListModel;

/**
 * Interface for RewardsListService.
 */
public interface RewardsListInterface {
    
    /**
     * Interface function call to find all rewards attached to a player. 
     * @param aPlayerId
     * @return
     */
    public List<RewardsListModel> findByPlayerId(Long aPlayerId);

    /**
     * Interface function call to find all rewards of a specific type attached to a player.
     * @param aPlayerId
     * @param aRewardTypeId
     * @return
     */
    public List<RewardsListModel> findByPlayerIdAndRewardTypeId(Long aPlayerId, Long aRewardTypeId);

    /**
     * Interface function call attach a new reward to a player.
     * @param aReward
     * @return
     */
    public RewardsListModel createRewardForPlayer(RewardsListModel aReward);

    /**
     * Interface function call to delete a reward by its id.
     * @param aRewardId
     * @return
     */
    public Long deleteReward(Long aRewardId);
}