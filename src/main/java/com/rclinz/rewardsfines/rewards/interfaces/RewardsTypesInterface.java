package com.rclinz.rewardsfines.rewards.interfaces;

import java.util.List;

import com.rclinz.rewardsfines.rewards.models.RewardsTypesModel;

/**
 * Interface for rewards types service.
 */
public interface RewardsTypesInterface {
    
    /**
     * Interface function call to find all reward types in the database.
     * @return
     */
    public List<RewardsTypesModel> findAll();

    /**
     * Interface function call to find a reward type by its id in the database.
     * @param aId
     * @return
     */
    public RewardsTypesModel findById(Long aId);

    /**
     * Interface function call to create a new reward type in the database.
     * @param aRewardType
     * @return
     */
    public RewardsTypesModel createRewardType(RewardsTypesModel aRewardType);

    /**
     * Interface function call to update an existing reward type by its id in the database.
     * @param aRewardType
     * @return
     */
    public RewardsTypesModel updateRewardType(RewardsTypesModel aRewardType);

    /**
     * Interface function call to delete a reward type by its id in the database.
     * @param aId
     * @return
     */
    public Long deleteRewardTypeById(Long aId);
}