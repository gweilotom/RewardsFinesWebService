package com.rclinz.rewardsfines.rewards.services;

import java.util.List;

import com.rclinz.rewardsfines.rewards.interfaces.RewardsListInterface;
import com.rclinz.rewardsfines.rewards.models.RewardsListModel;
import com.rclinz.rewardsfines.rewards.repositories.RewardsListRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to control access to 'rewards_list' table in the database.
 */
@Service
public class RewardsListService implements RewardsListInterface {

    // Rewards list repository for database access
    @Autowired
    private RewardsListRepository rewardsListRepo;

    /**
     * Service function call to find all rewards attached to a player.
     */
    @Override
    public List<RewardsListModel> findByPlayerId(Long aPlayerId) {
        return (List<RewardsListModel>) this.rewardsListRepo.findByPlayerid(aPlayerId);
    }

    /**
     * Service function call to find all rewards of a specific type attached to a player.
     */
    @Override
    public List<RewardsListModel> findByPlayerIdAndRewardTypeId(Long aPlayerId, Long aRewardTypeId) {
        return (List<RewardsListModel>) this.rewardsListRepo.findByPlayeridAndRewardid(aPlayerId, aRewardTypeId);
    }

    /**
     * Service function call to attach a new reward to a player.
     */
    @Override
    public RewardsListModel createRewardForPlayer(RewardsListModel aReward) {
        return (RewardsListModel) this.rewardsListRepo.save(aReward);
    }

    /**
     * Service function call to delete a specific reward from the database.
     */
    @Override
    public Long deleteReward(Long aRewardId) {
        return (Long) this.rewardsListRepo.deleteRewardById(aRewardId);
    }
}