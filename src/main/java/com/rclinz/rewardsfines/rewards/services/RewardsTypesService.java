package com.rclinz.rewardsfines.rewards.services;

import java.util.List;

import com.rclinz.rewardsfines.rewards.interfaces.RewardsTypesInterface;
import com.rclinz.rewardsfines.rewards.models.RewardsTypesModel;
import com.rclinz.rewardsfines.rewards.repositories.RewardsTypesRepository;

import org.springframework.stereotype.Service;

/**
 * Service to control access to 'rewards_types' table in the database.
 */
@Service
public class RewardsTypesService implements RewardsTypesInterface {

    // Rewards type repository for database access
    private RewardsTypesRepository rewardsTypesRepo;

    /**
     * Service call to find all reward types in the database.
     */
    @Override
    public List<RewardsTypesModel> findAll() {
        return (List<RewardsTypesModel>) this.rewardsTypesRepo.findAll();
    }

    /**
     * Service call to find a reward type by its id in the database.
     */
    @Override
    public RewardsTypesModel findById(Long aId) {
        return (RewardsTypesModel) this.rewardsTypesRepo.findRewardTypeById(aId);
    }

    /**
     * Service call to create a new reward type in the database.
     */
    @Override
    public RewardsTypesModel createRewardType(RewardsTypesModel aRewardType) {
        return (RewardsTypesModel) this.rewardsTypesRepo.save(aRewardType);
    }

    /**
     * Service call to update an existing reward type in the database.
     */
    @Override
    public RewardsTypesModel updateRewardType(RewardsTypesModel aRewardType) {
        return (RewardsTypesModel) this.rewardsTypesRepo.save(aRewardType);
    }

    /**
     * Service call to delete a reward type by its id in the database.
     */
    @Override
    public Long deleteRewardTypeById(Long aId) {
        return (Long) this.rewardsTypesRepo.deleteRewardTypeById(aId);
    }
}