package com.rclinz.rewardsfines.rewards.services;

import java.util.List;

import com.rclinz.rewardsfines.rewards.interfaces.RewardsOverviewInterface;
import com.rclinz.rewardsfines.rewards.models.RewardsOverviewModel;
import com.rclinz.rewardsfines.rewards.repositories.RewardsOverviewRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to control access to 'rewards_ovv_mvd' materialised view in the database.
 */
@Service
public class RewardsOverviewService implements RewardsOverviewInterface {

    // Rewards overview repository for database access
    @Autowired
    private RewardsOverviewRepository rewardsOverviewRepo;

    /**
     * Service function call to find all rewards overview data in the database.
     */
    @Override
    public List<RewardsOverviewModel> findAll() {
        return (List<RewardsOverviewModel>) this.rewardsOverviewRepo.findAll();
    }
    
}