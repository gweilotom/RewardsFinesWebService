package com.rclinz.rewardsfines.rewards.controllers;

import java.util.List;

import com.rclinz.rewardsfines.rewards.interfaces.RewardsOverviewInterface;
import com.rclinz.rewardsfines.rewards.models.RewardsOverviewModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API controller for accessing the rewards overview data in the database.
 */
@RestController
public class RewardsOverviewController {
    
    // Interface for reward overview access
    @Autowired
    private RewardsOverviewInterface rewardsOverviewInterface;

    /**
     * REST call to find all reward overview data in the database.
     * @return
     */
    @GetMapping(path = "/getRewardsOverview", produces = "application/json")
    public ResponseEntity<List<RewardsOverviewModel>> getRewardsOverview() {
        List<RewardsOverviewModel> rewardsOverview = this.rewardsOverviewInterface.findAll();
        if (rewardsOverview.size() > 0) {
            return ResponseEntity.ok(rewardsOverview);
        } else {
            return ResponseEntity.noContent().build();
        }
    }
}