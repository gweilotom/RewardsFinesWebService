package com.rclinz.rewardsfines.rewards.controllers;

import java.util.List;

import com.rclinz.rewardsfines.rewards.interfaces.RewardsTypesInterface;
import com.rclinz.rewardsfines.rewards.models.RewardsTypesModel;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API controller for access and manipulating the reward type data in the database.
 */
@RestController
public class RewardsTypesController {
    
    // Interface for reward type access
    private RewardsTypesInterface rewardsTypesInterface;

    /**
     * REST call to retrieve all reward types from the database.
     * @return
     */
    @GetMapping(path = "/getAllRewardTypes")
    public ResponseEntity<List<RewardsTypesModel>> getAllRewardTypes() {
        List<RewardsTypesModel> rewardTypes = this.rewardsTypesInterface.findAll();
        if (rewardTypes.size() > 0) {
            return ResponseEntity.ok(rewardTypes);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    /**
     * REST call to create a new, or update a existing reward type in the database.
     * @param aRewardType
     * @return
     */
    @PostMapping(path = "/createUpdateRewardType", consumes = "application/json", produces = "application/json")
    public ResponseEntity<RewardsTypesModel> createUpdateRewardType(@RequestBody RewardsTypesModel aRewardType) {
        RewardsTypesModel checkRewardType = this.rewardsTypesInterface.findById(aRewardType.getId());
        if (checkRewardType != null) {
            RewardsTypesModel updatedRewardType = this.rewardsTypesInterface.updateRewardType(aRewardType);
            if (updatedRewardType != null) {
                return ResponseEntity.ok(updatedRewardType);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
            }
        } else {
            RewardsTypesModel newRewardType = this.rewardsTypesInterface.createRewardType(aRewardType);
            if (newRewardType != null) {
                return ResponseEntity.ok(newRewardType);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_EXTENDED).build();
            }
        }
    }

    /**
     * REST call to delete a reward type from the database.
     * @param aId
     * @return
     */
    @DeleteMapping(path = "/deleteRewardType", produces = "application/json")
    public ResponseEntity<Long> deleteRewardType(@RequestParam(value = "id", required = true) Long aId) {
        RewardsTypesModel checkRewardType = this.rewardsTypesInterface.findById(aId);
        if (checkRewardType != null) {
            Long state = this.rewardsTypesInterface.deleteRewardTypeById(aId);
            if (state == 1) {
                return ResponseEntity.ok(state);
            } else if (state == 0) {
                return ResponseEntity.notFound().build();
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}