package com.rclinz.rewardsfines.rewards.controllers;

import java.util.List;

import com.rclinz.rewardsfines.rewards.interfaces.RewardsListInterface;
import com.rclinz.rewardsfines.rewards.models.RewardsListModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * REST API controller for accessing and manipulating reward list data in the database.
 */
@RestController
public class RewardsListController {
    
    // Interface for reward list access
    @Autowired
    private RewardsListInterface rewardsListInterface;

    /**
     * REST call to retrieve rewards for a player from the database.
     * @param aPlayerId
     * @param aRewardTypeId
     * @return
     */
    @GetMapping(path = "/getRewardsForPlayer", produces = "aaplication/json")
    public ResponseEntity<List<RewardsListModel>> getRewardsForPlayer(@RequestParam(value = "playerid") Long aPlayerId,
        @RequestParam(value = "rewardtypeid", required = false) Long aRewardTypeId) {
        if (aRewardTypeId != null) {
            List<RewardsListModel> rewardsList = this.rewardsListInterface.findByPlayerIdAndRewardTypeId(aPlayerId, aRewardTypeId);
            if (rewardsList.size() != 0) {
                return ResponseEntity.ok(rewardsList);
            } else {
                return ResponseEntity.noContent().build();
            }
        } else {
            List<RewardsListModel> rewardsList = this.rewardsListInterface.findByPlayerId(aPlayerId);
            if (rewardsList.size() != 0) {
                return ResponseEntity.ok(rewardsList);
            } else {
                return ResponseEntity.noContent().build();
            }
        }
    }

    /**
     * REST call to attach a new reward to a player.
     * @param aPlayerId
     * @param aRewardTypeId
     * @param aMultiplier
     * @param aBonus
     * @return
     */
    @PostMapping(value="/rewardPlayer", consumes = "application/json", produces = "application/json")
    public ResponseEntity<RewardsListModel> rewardPlayer(@RequestBody RewardsListModel newReward) {
        newReward = this.rewardsListInterface.createRewardForPlayer(newReward);
        if (newReward != null) {
            return ResponseEntity.ok(newReward);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_EXTENDED).build();
        }
    }
    
    /**
     * REST call to delete a reward from the database.
     * @param aRewardId
     * @return
     */
    @DeleteMapping(path = "/deleteReward", produces = "application/json")
    public ResponseEntity<Long> deleteReward(@RequestParam(value = "rewardid", required = true) Long aRewardId) {
        Long state = this.rewardsListInterface.deleteReward(aRewardId);
        if (state == 1) {
            return ResponseEntity.ok(state);
        } else if (state == 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}