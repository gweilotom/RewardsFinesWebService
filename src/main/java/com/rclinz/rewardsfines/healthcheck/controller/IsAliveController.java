package com.rclinz.rewardsfines.healthcheck.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API controller for accessing fines overview data in the database.
 */
@RestController
public class IsAliveController {
    /*
     * @GetMapping("/hello") public String hello(@RequestParam(value = "name",
     * defaultValue = "World") String name) { return new
     * HelloWorldModel(counter.incrementAndGet(), String.format(template, name)); }
     */

    /**
     * Healthcheck function for REST webservice.
     * @return
     */
    @GetMapping(path = "/hello")
    public String hello() {
        return "Rewards and fines REST webservice is running!";
    }
}